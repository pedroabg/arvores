<!DOCTYPE html>
<!--
Pedro
-->
<html>
    <head>
        <link href="style.css" rel="stylesheet" />
        <meta name="viewport" content="user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, minimal-ui">
        <title>Animated BFS</title>
        <script src="jquery.js"></script>
        <script src="cytoscape.js"></script>
        <script src="avl.js"></script>
<!--        <script src="https://cdn.rawgit.com/cpettitt/dagre/v0.7.4/dist/dagre.min.js"></script>
        <script src="https://cdn.rawgit.com/cytoscape/cytoscape.js-dagre/1.1.2/cytoscape-dagre.js"></script>-->
        <script type="text/javascript">

            //printTree(avl, nos, arestas);

            //console.log(nos);
            function cyFunc() {
                var cy;
                this.cyFunc2 = $(function () {
                    this.cy = window.cy = cytoscape({
                        container: document.getElementById('cy'),
                        boxSelectionEnabled: false,
                        autounselectify: true,
                        layout: {
                            name: 'breadthfirst',
                            directed: true,
                            //roots: avl.node,
                            padding: 10
                        },
                        style: [
                            {
                                selector: 'node',
                                style: {
                                    'content': 'data(id)',
                                    'text-opacity': 0.5,
                                    'text-valign': 'center',
                                    'text-halign': 'center',
                                    'background-color': '#11479e'
                                }
                            }, {
                                selector: '.nil',
                                style: {
                                    'content': 'nil',
                                    'text-opacity': 1.0,
                                    'text-valign': 'center',
                                    'text-halign': 'center',
                                    'background-color': 'white'
                                }
                            }, {
                                selector: '.selected',
                                style: {
                                    'content': 'data(id)',
                                    'text-opacity': 1.0,
                                    'text-valign': 'center',
                                    'text-halign': 'center',
                                    'background-color': 'green'
                                }
                            },
                            {
                                selector: 'edge',
                                style: {
                                    'width': 4,
                                    'target-arrow-shape': 'tee',
                                    'line-color': '#9dbaea',
                                    'target-arrow-color': '#9dbaea'
                                }
                            }
                        ],
                        elements: {
                            nodes: nos,
                            edges: arestas
                        }
                    });

                });
            }
            ;



            //cyFunc();

            function addElement(n) {
                var value = parseInt(n);
                if (!avl) {
                    avl = new AVLTree(value);
                    printTree(avl, nos, arestas);
                    cyFunc();
                }
                else {

                    if (avl.findBest(value).node) {
                        findElement(value);
                    } else {
                        avl.add(value);
                        //console.log(avl);
                        nos = [];
                        arestas = [];
                        printTree(avl, nos, arestas);
                        console.log(arestas);
                        cyFunc();

                    }
                }

            }
            function removeElement(n) {
                var value = parseInt(n);
                if (avl.findBest(value)) {
                    nos = [];
                    arestas = [];

                    if (avl.remove(value)) {
                        printTree(avl, nos, arestas);
                    } else{
                        avl = null;
                    }
                    cyFunc();
                    //console.log(avl);
                } else
                    console.log("elemento não encontrado");
            }
            function findElement(n) {
                var value = parseInt(n);
                var no = avl.findBest(value);
                nos = [];
                arestas = [];
                printTree(avl, nos, arestas);
//                printNode(no);
                console.log(no);
                for (var i = 0; i < nos.length; i++) {
                    if (nos[i].data.id == value) {
                        nos[i].classes = "selected";
                        //console.log(nos[i]);
                    }
                }
                cyFunc();
            }

            function printNode(no) {
                console.log("Chave: " + no.node);
                console.log("Altura: " + (no.depth - 1));
                if (no.left != null)
                    console.log("Nó Esquerdo: " + no.left.node);
                if (no.right != null)
                    console.log("Nó Direito: " + no.right.node);

            }


        </script>
    </head>
</script>

</head>
<body>
    <?php
// put your code here
    ?>
    <div>
        <input id="Insert_Input" type="Text" value="" name="" size="4"/>
        <input type="Button" onclick="addElement(getElementById('Insert_Input').value)" value="Insert" name="Insert"/>
        <br/>
        <input id="Find_Input" type="Text" value="" name="" size="4"/>
        <input type="Button" onclick="findElement(getElementById('Find_Input').value)" value="Find" name="Insert"/>
        <br/>
        <input id="Remove_Input" type="Text" value="" name="" size="4"/>
        <input type="Button" onclick="removeElement(getElementById('Remove_Input').value)" value="Remove" name="Insert"/>
        <br/>

    </div>
    <div id="cy"></div>
    <!--<div id="arvore"></div>-->


    <script type="text/javascript">



    </script>
</body>
</html>
