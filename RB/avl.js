// AVLTree ///////////////////////////////////////////////////////////////////
//   This file is originally from the Concentré XML project (version 0.2.1)
//   Licensed under GPL and LGPL
//
//   Modified by Jeremy Stephens.

// Pass in the attribute you want to use for comparing

var nos = [];
var arestas = [];

function AVLTree(n, attr) {
    this.init(n, attr);
}

AVLTree.prototype.init = function (n, attr) {
    this.attr = attr;
    this.left = null;
    this.right = null;
    this.node = n;
    this.depth = 1;
    this.elements = [n];
};

AVLTree.prototype.balance = function () {
    var ldepth = this.left == null ? 0 : this.left.depth;
    var rdepth = this.right == null ? 0 : this.right.depth;

    if (ldepth > rdepth + 1) {
        // LR or LL rotation
        var lldepth = this.left.left === null ? 0 : this.left.left.depth;
        var lrdepth = this.left.right === null ? 0 : this.left.right.depth;

        if (lldepth < lrdepth) {
            // LR rotation consists of a RR rotation of the left child
            this.left.rotateRR();
            // plus a LL rotation of this node, which happens anyway
        }
        this.rotateLL();
    } else if (ldepth + 1 < rdepth) {
        // RR or RL rorarion
        var rrdepth = this.right.right === null ? 0 : this.right.right.depth;
        var rldepth = this.right.left === null ? 0 : this.right.left.depth;

        if (rldepth > rrdepth) {
            // RR rotation consists of a LL rotation of the right child
            this.right.rotateLL();
            // plus a RR rotation of this node, which happens anyway
        }
        this.rotateRR();
    }
};

AVLTree.prototype.rotateLL = function () {
    // the left side is too long => rotate from the left (_not_ leftwards)
    var nodeBefore = this.node;
    var elementsBefore = this.elements;
    var rightBefore = this.right;
    this.node = this.left.node;
    this.elements = this.left.elements;
    this.right = this.left;
    this.left = this.left.left;
    this.right.left = this.right.right;
    this.right.right = rightBefore;
    this.right.node = nodeBefore;
    this.right.elements = elementsBefore;
    this.right.updateInNewLocation();
    this.updateInNewLocation();
};

AVLTree.prototype.rotateRR = function () {
    var nodeBefore = this.node;
    var elementsBefore = this.elements;
    var leftBefore = this.left;
    this.node = this.right.node;
    this.elements = this.right.elements;
    this.left = this.right;
    this.right = this.right.right;
    this.left.right = this.left.left;
    this.left.left = leftBefore;
    this.left.node = nodeBefore;
    this.left.elements = elementsBefore;
    this.left.updateInNewLocation();
    this.updateInNewLocation();
};

AVLTree.prototype.updateInNewLocation = function () {
    this.getDepthFromChildren();
};

AVLTree.prototype.getDepthFromChildren = function () {
    this.depth = this.node === null ? 0 : 1;
    if (this.left !== null) {
        this.depth = this.left.depth + 1;
    }
    if (this.right !== null && this.depth <= this.right.depth) {
        this.depth = this.right.depth + 1;
    }
};

AVLTree.prototype.compare = function (n1, n2) {
    v1 = n1;
    v2 = n2;
    if (v1 == v2) {
        return 0;
    }
    if (v1 < v2) {
        return -1;
    }
    return 1;
};

AVLTree.prototype.add = function (n) {
    var o = this.compare(n, this.node);
    if (o == 0) {
        this.elements.push(n);
        return false;
    }

    var ret = false;
    if (o === -1) {
        if (this.left == null) {
            this.left = new AVLTree(n, this.attr);
            ret = true;
        } else {
            ret = this.left.add(n);
            if (ret) {
                this.balance();
            }
        }
    } else if (o == 1) {
        if (this.right == null) {
            this.right = new AVLTree(n, this.attr);
            ret = true;
        } else {
            ret = this.right.add(n);
            if (ret) {
                this.balance();
            }
        }
    }

    if (ret) {
        this.getDepthFromChildren();
    }
    return ret;
};

// Given the beginning of a value, return the elements if there's a match
AVLTree.prototype.findBest = function (value) {
//    var substr = this.node[this.attr].substr(0, value.length).toLowerCase();
//    var value = value.toLowerCase();

    var node = this.node;


    if (value < node) {
        if (this.left != null) {
            return this.left.findBest(value);
        }
        return [];
    }
    else if (value > node) {
        if (this.right != null) {
            return this.right.findBest(value);
        }
        return [];
    }
    return this;
}



AVLTree.prototype.getNextBigger = function () {
    var aux = this;
    if (aux.left === null) {
        return aux;
    }
    else {
        return aux.left.getNextBigger();
    }
};


AVLTree.prototype.setNextBigger = function (pai) {
    var aux = this;
    if (aux.left === null) {
        return true;
    }
    else {
        if (this.left.setNextBigger(this)) {
            this.left = null;
            return false;
        } else
            return this.left.setNextBigger(this);
    }
};

AVLTree.prototype.delNextBigger = function (pai) {
    var aux = this;
    if (aux.left === null) {
        //pai.right = aux.right;
        //console(pai);
        if (aux.node < pai.node) {
            //console(pai);
            pai.left = aux.right;
        }
        if (aux.node >= pai.node) {
            //console(pai);
            pai.right = aux.right;
        }
        
        pai.balance();
        
    }
    else {
        return aux.left.delNextBigger(this);
    }
};


AVLTree.prototype.remove = function (value, pai) {
    var aux;

    var ret = false;

    if (this.node === null)
        return null;

    if (this.node === value) {

        aux = this;
        // nao possui filhos
        /* se não tiver filho na esquerda */
        if (this.left === null && this.right !== null) {
            aux = this.right; /*então o filho da direita substitui */
            this.node = aux.node;
            this.right = null;
            ret = true;
        }
        /* se não tem filho a direita */
        if (this.right === null && this.left !== null) {
            aux = this.left;
            this.node = aux.node;
            this.left = null;
            ret = true;
        }
        // dois filhos
        if (this.right !== null && this.left !== null)
        { /*  possui os dois filhos */
            // pegar o menor maior , sucessor
            aux = this.right.getNextBigger();
            this.node = aux.node;
            this.right.delNextBigger(this);
            ret = true;
        }
        if (this.left === null && this.right === null) {
            if (this.node < pai.node) {
                pai.left = null;
            } else {
                pai.right = null;
            }
            ret = true;
        }
    }
    
    else {
        // Não encontrou ainda. Procurar
        if (value < this.node) {
            return this.left.remove(value, this);
        } else {
            return this.right.remove(value, this);
        }
    }


    if (ret) {
        this.getDepthFromChildren();
//        console.log(" balaaaanço" + this.node);
//        console.log(this);
        if (pai != null)
            pai.balance();
        else
            this.balance();
        
    }
    return ret;


}

var inserir = [1,2,3,4,5,6,7];

var avl = new AVLTree(1);

for (i = 1 ; i<50 ; i += 3)
    avl.add(i);

//for (i = 0; i < inserir.length; i++)
//    avl.add(inserir[i]);

//avl.remove(3);

//
//console.log(avl.findBest(3));
//console.log(avl.findBest(7));






console.log(avl);
//            console.log(avl.elements);

function printTree(tree, nos, arestas) {
    nos.push({group: "nodes", data: {id: tree.node}});
    printTreeNodes(tree, nos, arestas);

}

function printTreeNodes(tree, nos, arestas) {

    if (tree === null) {

    } else {



        if (tree.left !== null) {
            nos.push({group: "nodes", data: {id: tree.left.node}});
            arestas.push({group: "edges", data: {id: "e_" + tree.node + "_" + tree.left.node, source: tree.node, target: tree.left.node}});
            //document.getElementById("arvore").innerHTML += "---" + tree.left.node;
        } else {
            nos.push({group: "nodes", classes: "nil", data: {id: tree.node + "-n-l"}});
            arestas.push({group: "edges", data: {id: "e_" + tree.node + "n-l", source: tree.node, target: tree.node + "-n-l"}});
        }
        if (tree.right !== null) {
            nos.push({group: "nodes", data: {id: tree.right.node}});
            arestas.push({group: "edges", data: {id: "e_" + tree.node + "_" + tree.right.node, source: tree.node, target: tree.right.node}});
            //document.getElementById("arvore").innerHTML += "---" + tree.right.node;
        } else {
            nos.push({group: "nodes", classes: "nil", data: {id: tree.node + "-n-r"}});
            arestas.push({group: "edges", data: {id: "e_" + tree.node + "n-r", source: tree.node, target: tree.node + "-n-r"}});
        }



        printTreeNodes(tree.left, nos, arestas);
        printTreeNodes(tree.right, nos, arestas);
    }




}

        